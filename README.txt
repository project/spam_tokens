
Spam_Tokens is an add-on to the Spam module. It allows the Spam administrator to control
the tokens that are used by the Spam filters to determine whether or not content is spam.

Required Modules
----------------

The Spam_Tokens module requires the contributed Spam module.

Installation
------------
Normal module installation procedures.

Settings
--------
There are no settings for this module.

Permissions
-----------
There are no new permissions for this module.

Menu Items
----------
Tokens - is a tab on the Spam administration (Administer >> Site Configuration >> Spam) page.

Using the Module
----------------
On the Spam_Tokens page you will have a list of all current tokens.

There is a link at the top to add new tokens.

Next (if you have enough tokens yet) you will find a box with the first letters of tokens.
Clicking on one of those letters will restrict the display to only tokens beginning with
that letter.

Last, you will find the [possibly filtered] list of tokens. Each has a link to edit that
token. The edit function also is used to delete the token.

* The "Spam" and "Not Spam" columns show how many times the token has been seen in content
  that ended up being marked as spam or not.

* "Probablility" is the likelihood that a term is an indicator of spam. The "average" value
  for this is 50 (percent). Values below this indicate that the token is less likely to be
  spam; values above it are more likely to be spam. The maximum value is 99, which will
  definitely mark the content as spam. A value of zero (0) indicates that the token should
  be ignored entirely. The farther the probability is away from 50, the more "interesting"
  the token becomes. The Spam filter will use a configurable number (the default is 15) of
  the most interesting tokens. [This number is shown at the top of the page.]

* "Last seen" tells you when the token was last encountered in content.

